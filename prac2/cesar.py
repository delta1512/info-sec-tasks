from string import ascii_uppercase as au


def cipher(P: str, k: int):
    '''
    Encrypts a string P using key k using a cesar cipher.

    This function handles any non-alphabetic characters without transformation.
    '''
    P = P.upper()
    cipher_txt = ''
    for c in P:
        if c not in au:
            cipher_txt += c
        else:
            cipher_txt += au[(au.index(c)+k) % 26]
    return cipher_txt


def decipher(C: str, k: int):
    '''
    Decrypts a cesar cipher text C using key k. Same requirements as above.
    '''
    C = C.upper()
    plain_txt = ''
    for c in C:
        if c not in au:
            plain_txt += c
        else:
            plain_txt += au[(au.index(c)-k) % 26]
    return plain_txt


def get_integer_key():
    # Collect and validate key input
    key = input('Enter a key: ')
    while True:
        try:
            return int(key)
        except ValueError:
            key = input('Please enter an integer key: ')


if __name__ == '__main__':
    choice = ''
    while choice.lower() != 'exit':
        choice = input('Encrypt (e) decrypt (d) exit (exit): ')
        if choice.lower() == 'e':
            # Collect plain text
            plain_txt = input('Enter some text to encrypt:\n')
            key = get_integer_key()

            print('Encrypting your text...')
            print(cipher(plain_txt, key))
        elif choice.lower() == 'd':
            # Collect cipher text
            cipher_txt = input('Enter some text to decrypt:\n')
            key = get_integer_key()
            print('Decrypting your text...')
            print(decipher(cipher_txt, key))
        elif choice.lower() != 'exit':
            print('Bad input, Try again.')
