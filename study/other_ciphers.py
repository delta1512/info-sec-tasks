import math
import random as r
from string import ascii_lowercase

import numpy as np


PLAYFAIR_TEMPLATE = 'abcdefghiklmnopqrstuvwxyz'


def idx_1D_to_2D(idx: int, rows: int, cols: int):
    return math.floor(idx / cols), round(idx % rows)


def idx_2D_to_1D(row: int, col: int, rows: int, cols: int):
    return (row * cols) + col


def get_mono_map(in_domain: list, out_domain: list):
    assert len(in_domain) == len(out_domain)

    map = {}
    for i, in_char in enumerate(in_domain):
        map[in_domain] = out_domain[i]

    return map


def split_and_fill(s: str, filler: str):
    next_included = False
    for i in range(len(s)):
        if next_included:
            next_included = False
            continue

        block = s[i]

        if i + 1 >= len(s):
            block += filler
        elif s[i+1] == block:
            block += filler
        else:
            block += s[i+1]
            next_included = True

        yield tuple(block)


def encrypt_playfair(key: str, p: str, filler: str='z'):
    '''
    I is assumed to be I/J
    '''
    p = p.lower().replace('j', 'i')
    key = key.replace('j', 'i')
    alpha = PLAYFAIR_TEMPLATE
    final_k_str = ''
    ciphertext = ''

    for i in range(len(key)):
        c = key[i]
        if not c in key[0:i]:
            final_k_str += c
            alpha = alpha.replace(c, '')

    final_k_str += alpha
    row = col = 0

    for row in range(5):
        s = ''
        for col in range(5):
            s += str(idx_2D_to_1D(row, col, 5, 5)) + final_k_str[idx_2D_to_1D(row, col, 5, 5)] + '  '
        #print(s)


    for block in split_and_fill(p, filler):
        rowa, cola = idx_1D_to_2D(final_k_str.index(block[0]), 5, 5)
        rowb, colb = idx_1D_to_2D(final_k_str.index(block[1]), 5, 5)

        #print(block)
        #print('{}'.format((rowa, cola)))
        #print('{}'.format((rowb, colb)))

        if rowa == rowb:
            cola = (cola + 1) % 5
            colb = (colb + 1) % 5
        elif cola == colb:
            rowa = (rowa + 1) % 5
            rowb = (rowb + 1) % 5
        else:
            tmp = cola
            cola = colb
            colb = tmp

        ciphertext += final_k_str[idx_2D_to_1D(rowa, cola, 5, 5)]
        ciphertext += final_k_str[idx_2D_to_1D(rowb, colb, 5, 5)]

        #print('{}'.format((rowa, cola)))
        #print('{}'.format((rowb, colb)))
        #print(idx_2D_to_1D(rowa, cola, 5, 5))
        #print(idx_2D_to_1D(rowb, colb, 5, 5))
        #input()

    print(ciphertext)


def encrypt_hill(key: str, p: str):
    assert len(key) == len(p)**2

    p = p.lower()
    p_matrix = np.array([[ord(c)-97 for c in p]])
    n = len(p)
    key_matrix = np.array([[ord(c)-97 for c in key[i:i+n]] for i in range(0, len(key), n)])

    print([i%26 for i in np.dot(p_matrix, key_matrix)[0]])


def encrypt_vigenere(key: str, p: str):
    n = math.ceil(len(p)/len(key))
    key = key*n
    ciphertext = ''

    for i in range(len(p)):
        ciphertext += ascii_lowercase[(ord(p[i])-97 + ord(key[i])-97) % 26]

    print(ciphertext)
