'''
sdes.py by Marcus Belcastro (19185398)

This file implements a library and command-line application for
encrypting and decrypting binary encoded into ascii strings.

To use, simply run:
python sdes.py
'''

from lib.b_string import BString
from lib.p_table import *
from lib.s_box import *

# Prepare constants to be used
IP = PTable(SDES_IP)
EP = PTable(SDES_EP)
P4 = PTable(SDES_P4)
S0 = SBox(SDES_S0)
S1 = SBox(SDES_S1)
IP_1 = IP.get_inverse()

P10 = PTable(SDES_P10)
P8 = PTable(SDES_P8)


def encrypt_sdes(inp: str, key: str):
    # Convert the strings to BStrings
    p = BString(inp)
    k = BString(key)

    assert len(p) == 8, 'plaintext must be 8-bits long'
    assert len(k) == 10, 'key must be 10-bits long'

    # Perform the initial permutation
    p = IP.permute(p)
    # Get the generated keys
    keys = list(key_gen(k))
    # Perform the rounds
    lhs, rhs = do_sdes_round(p[0:4], p[4:8], keys[0])
    lhs, rhs = do_sdes_round(rhs, lhs, keys[1])
    # Concatenate and permute with the inverse initial permutation
    c = IP_1.permute(lhs + rhs)
    return c


def decrypt_sdes(inp: str, key: str):
    # Convert the strings to BStrings
    c = BString(inp)
    k = BString(key)

    assert len(c) == 8, 'ciphertext must be 8-bits long'
    assert len(k) == 10, 'key must be 10-bits long'

    # Perform the initial permutation
    c = IP.permute(c)
    # Get the generated keys
    keys = list(key_gen(k))
    # Perform the rounds
    lhs, rhs = do_sdes_round(c[0:4], c[4:8], keys[1])
    lhs, rhs = do_sdes_round(rhs, lhs, keys[0])
    # Concatenate and permute with the inverse initial permutation
    p = IP_1.permute(lhs + rhs)
    return p


def do_sdes_round(p_lhs: BString, p_rhs: BString, k: BString):
    assert len(p_lhs) == 4, 'length of the left half is not to spec'
    assert len(p_rhs) == 4, 'length of the right half is not to spec'
    assert len(k) == 8, 'length of the generated key is not to spec'
    # Perform the expanding permutation
    p_working = EP.permute(p_rhs)
    # XOR with the key
    p_working = p_working ^ k
    # Split into halves
    p_working_lhs = p_working[0:4]
    p_working_rhs = p_working[4:8]
    # LHS S-box
    p_working_lhs = S0.sub_bin(p_working_lhs)
    p_working_rhs = S1.sub_bin(p_working_rhs)
    # Merge the two halves
    p_working = p_working_lhs + p_working_rhs
    # Perform P4
    p_working = P4.permute(p_working)
    # Return the two halves at the end of the round
    return p_working ^ p_lhs, p_rhs


def key_gen(k: BString):
    assert len(k) == 10, 'length of the raw key is not to spec'
    # Perform P10
    curr_k = P10.permute(k)
    # Split the key in half and rotate left
    k_lhs = curr_k[0:5] << 1
    k_rhs = curr_k[5:10] << 1

    # Merge the two halves
    curr_k = k_lhs + k_rhs

    # Return k1
    yield P8.permute(curr_k)

    # Perform the next left rotations
    k_lhs = k_lhs << 2
    k_rhs = k_rhs << 2

    # Merge the two halves
    curr_k = k_lhs + k_rhs

    #return k2
    yield P8.permute(curr_k)


# Entry point when running from the terminal
if __name__ == '__main__':
    choice = ''

    while choice != 'exit':
        choice = input('Encrypt (E), decrypt (D) or exit (exit): ')

        try:
            # Encryption
            if choice.lower() == 'e':
                p = input('Input 8-bit plaintext: ')
                k = input('Input 10-bit key: ')
                print('Ciphertext is {}'.format(encrypt_sdes(p, k)))

            # Decryption
            elif choice.lower() == 'd':
                c = input('Input 8-bit ciphertext: ')
                k = input('Input 10-bit key: ')
                print('Plaintext is {}'.format(decrypt_sdes(c, k)))

        # Catch any assertion errors and inform the user
        except AssertionError as e:
            print('ERROR: {}'.format(e))
