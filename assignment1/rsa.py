'''
rsa.py by Marcus Belcastro (19185398)

This file implements a library and command-line application for
encrypting and decrypting integers using the RSA algorithm.

To use, simply run:
python rsa.py
'''

from lib import specmath as sm


def encrypt_rsa(p: int, q: int, e: int, m: int):
    assert p != q, 'p and q must be different'
    phi_n = (p - 1) * (q - 1)
    assert 1 < e < phi_n, 'e must be between 1 and phi_n'
    n = p * q
    assert 1 < m < n, 'M must be between 1 and n'
    assert sm.gcd(phi_n, e) == 1, 'e is not relatively prime to phi_n'
    assert sm.is_prime(p), 'p is not prime'
    assert sm.is_prime(q), 'q is not prime'

    # Perform the encryption with power modulo
    c = sm.fast_mod(m, e, n)
    # Find d
    d = sm.get_mult_inverse(phi_n, e)

    return c, n, d


def decrypt_rsa(c: int, n: int, d: int):
    return sm.fast_mod(c, d, n)


# Entry point when running from the terminal
if __name__ == '__main__':
    choice = ''

    while choice != 'exit':

        choice = input('Encrypt (E), decrypt (D) or exit (exit): ')

        try:

            # Encryption
            if choice.lower() == 'e':
                data = input('Input separated by spaces p, q, e, m: ')
                p, q, e, m = [int(i) for i in data.split()]
                c, n, d = encrypt_rsa(p, q, e, m)
                print('C={}, n={}, d={}'.format(c, n, d))

            # Decryption
            elif choice.lower() == 'd':
                data = input('Input separated by spaces c, n, d: ')
                c, n, d = [int(i) for i in data.split()]
                print('M is {}'.format(decrypt_rsa(c, n, d)))

        # Catch assertion errors and present it to the user
        except AssertionError as e:
            print('ERROR: {}'.format(e))

        # Print a message when the user doesn't type the right amount of values
        except ValueError:
            print('Please type all the required arguments as numbers separated by spaces')
