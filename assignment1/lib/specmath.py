'''
specmath.py by Marcus Belcastro (19185398)

This file implements a high-level mathematics library which contains
specialised formulas, processes and equations.
'''

import math

from . import b_string
from lib.b_string import BString


def gcd(a: int, b: int):
    '''
    Finds the greatest common divisor of a and b.
    '''
    # To ensure correct output, swap the variables to be in order if b > a
    if b > a:
        tmp = a
        a = b
        b = tmp

    while b != 0:
        r = a % b
        a = b
        b = r

    return a


def is_relatively_prime(a: int, b: int):
    '''
    Checks whether a and b are relatively prime. This means that their gcd=1
    '''
    return gcd(a, b) == 1


def is_prime(n: int):
    '''
    Checks whether n is prime.

    This funtion uses fermat's theorum to filter out non-primes.
    '''
    # Bounds checking
    if n <= 1:
        return False
    # 2 is the only even prime number
    elif n == 2:
        return True
    # Even numbers are not prime
    elif n % 2 == 0:
        return False
    # Check Fermat's theorum
    elif not fermat_prime(n):
        return False
    # Perform a factorisation
    else:
        # Loop up to the sqrt of n and only check odd numbered factors
        # sqrt(n) is the maximal unique integer that could divide n
        for i in range(3, int(math.sqrt(n)), 2):
            if n % i == 0:
                return False
    return True


def get_mult_inverse(a: int, b: int):
    '''
    Extended GCD function to find a multiplicative inverse of b with respect to
    the modulo a.
    '''
    # To ensure correct output, swap the variables to be in order if b > a
    if b > a:
        tmp = a
        a = b
        b = tmp

    # a1 and b1 are omitted for efficiency reasons
    a2 = 0
    a3 = a
    b2 = 1
    b3 = b

    while b3 < 0 or b3 > 1:
        q = math.floor(a3 / b3)
        t2 = b2
        t3 = b3
        b2 = a2 - (b2 * q)
        b3 = a3 - (b3 * q)
        a2 = t2
        a3 = t3

    if b3 == 0:
        raise ValueError('{} does not have a multiplicative inverse under modulo {}'.format(b, a))
    else:
        # Make any negative values positive
        if b2 < 0:
            b2 = (b2 + a) % a
        return b2


def fast_mod(base: int, exp: int, mod: int):
    '''
    Modular arithmetic using the square and multiply algorithm.
    '''
    bin_exp = BString(exp)
    bin_exp = bin_exp[bin_exp.index('1'):] # Align to MSB

    # Calculate the incrementing powers % mod
    powers = []
    curr_exp = 2
    end = 2 ** (len(bin_exp) - 1)

    powers.append(base % mod) # Fix off-by-one bug

    while curr_exp <= end:
        powers.append((powers[-1] ** 2) % mod)
        curr_exp += curr_exp

    # Multiply and sum the corresponding powers of 2 according to the binary
    result = 1
    for bit in bin_exp:
        curr_mod = powers.pop()
        if bit == '1':
            result *= curr_mod

    # Perform the final modulo
    return result % mod


def fermat_prime(p: int):
    '''
    Checks according to Fermat's little theorum as to whether this number is a
    probable prime.
    '''
    return (2**(p-1)) % p == 1
