'''
p_table.py by Marcus Belcastro (19185398)

This file is a library that implements a substitution box.
'''

from . import table
from lib.table import *


# P-TABLE Constants
SDES_P10 = [2, 4, 1, 6, 3, 9, 0, 8, 7, 5]
SDES_P8 = [5, 2, 6, 3, 7, 4, 9, 8]
SDES_EP = [3, 0, 1, 2, 1, 2, 3, 0]
SDES_P4 = [1, 3, 2, 0]
SDES_IP = [1, 5, 2, 0, 3, 7, 4, 6]


class PTable(Table):
    def __init__(self, table_def: list, idx: int=0):
        '''
        table_def: A list that defines the permutation table
        '''
        Table.__init__(self, table_def, idx)

    def get_inverse(self):
        '''
        Gets the inverse of a permutation table.
        Only does so if the table is invertable.
        '''
        assert self.is_invertible(), 'attempted to invert a non-invertable table'

        out_table = []

        for i in range(self.out_len):
            out_table.append(self.table.index(i))

        return PTable(out_table)

    def permute(self, inp):
        '''
        Performs a permutation on any iterable input.
        If input is smaller than that supported by the pTable, AssertionError
        If input is larger, any extra data is left unused.
        '''
        assert len(inp) >= self.in_len, 'input too small for PTable'
        in_str = list(inp)
        out_str = []

        for i in range(self.out_len):
            out_str.append(in_str[self.table[i]])

        return type(inp)(''.join(out_str))
