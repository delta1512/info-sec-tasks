'''
s_box.py by Marcus Belcastro (19185398)

This file is a library that implements a substitution box.
'''

import math

from . import b_string
from lib.b_string import *


SDES_S0 = [
    [1, 0, 3, 2],
    [3, 2, 1, 0],
    [0, 2, 1, 3],
    [3, 1, 3, 2]
]

SDES_S1 = [
    [0, 1, 2, 3],
    [2, 0, 1, 3],
    [3, 0, 1, 0],
    [2, 1, 0, 3]
]


class SBox:
    def __init__(self, box_def: list):
        '''
        box_def: a 2D list that implements the substitution box.
        '''
        self.box = box_def
        # find the dimensions to do some calculations and assertions
        height = len(self.box)
        width = len(self.box[0])
        max_out_num = 0

        assert width > 2, 'width of the table is too small'
        assert height > 2, 'height of the table is too small'

        for row in self.box:
            assert len(row) == width, 'found a row that is smaller than the width'
            for n in row:
                if n > max_out_num:
                    max_out_num = n

        self.out_len = math.ceil(math.log(max_out_num, 2)/8) + 1

    def sub(self, row: int, col: int):
        '''
        Substitute a row and column number
        '''
        return self.box[row][col]

    def sub_bin(self, inp: BString):
        '''
        Substitute a binary string to the row and column of the substitution table.
        '''
        assert len(inp) > 2, 'substitution binary is too small'
        # The row is always defined as the ends of the binary string
        row = BString(inp[0] + inp[-1]).to_int()
        # The column is always defined as everything in between
        col = BString(inp[1:-1]).to_int()
        return BString(self.sub(row, col))[-self.out_len:]

    def __str__(self):
        out = ''
        for row in self.box:
            out += str(row) + '\n'
        return out

    def __repr__(self):
        return self.__str__()
