'''
table.py by Marcus Belcastro (19185398)

This file implements a library that defines a basic 1D table.
This is the parent class for all permutation tables.
'''

class Table:
    '''
    Table: A definition of a basic 1D table.
    '''

    def __init__(self, table_def: list, idx: int=0):
        '''
        table_def - list of integers that represent the table.
        idx - whether the list is indexed starting at 0, 1 or another offset.
        '''
        # table will always be indexed at 0
        self.table = [i-idx for i in table_def]
        self.in_len = max(self.table)+1
        self.out_len = len(self.table)

    def is_invertible(self):
        '''
        An invertible Table is a bijective function.
        This means:
            - The input size must equal the output size
            - there should be no duplicates in the table
            - the elements of the table should be those in the range of the length
        '''
        return  (
            self.in_len == self.out_len and
            len(set(self.table)) == self.out_len and
            len(set(self.table)) == len(set(self.table) | set(range(self.out_len)))
        )
