'''
b_string.py by Marcus Belcastro (19185398)

This file is a library that implements binary strings encoded as ASCII.
An example of a BString is "10110100001"
'''

import re
import math


BIN_REGEX = '^[0,1]{1,}$'


def blockify(iterable, size):
    '''
    Splits an iterable into chunks, does not pad the results.
    '''
    prev = 0
    next = size
    while prev < len(iterable):
        yield iterable[prev:next]
        prev = next
        next += size


def pad(iterable, to_size, padding):
    '''
    Pads for big-endian
    '''
    if len(iterable) < to_size:
        for i in range(to_size - len(iterable)):
            iterable = padding + iterable
    return iterable


class BString:
    '''
    BString: Defines binary encoded as an ASCII string of 1s and 0s.
    Any integer interpretation of a BString is in big-endian.
    '''

    def __init__(self, s, one: str='1', zero: str='0'):
        '''
        Change one and zero appropriately for a different ASCII base.
        '''
        if type(s) is bytes:
            self.bin_string = self.from_bytes(s)
        elif type(s) is int:
            self.bin_string = self.from_int(s)
        elif type(s) is str:
            self.bin_string = s.replace(one, '1').replace(zero, '0')
        elif type(s) is BString:
            self.bin_string = s.bin_string
        else:
            raise ValueError('Invalid conversion of {} to BString'.format(type(s)))
        assert re.match(BIN_REGEX, self.bin_string), 'detected bad input for the BString'

        self.iter_i = 0

    def to_bytes(self):
        out_bytes = b''
        for byte in blockify(self.bin_string, 8):
            current_int = 0
            mask = 0x80
            byte = pad(byte, 8, '0')

            for bit in byte:
                # Only need to add when there is a 1
                if bit == '1':
                    current_int += mask

                mask = mask >> 1

            out_bytes += current_int.to_bytes(1, 'big')

        return out_bytes

    def from_bytes(self, b: bytes):
        in_int = int.from_bytes(b, 'big')
        mask = 0x1
        out_str = ''
        for i in range(len(b)*8):
            bit = in_int & mask # Get the current bit
            bit = bit >> i  # Shift it so it is either 1 or 0

            if bit == 0:
                out_str += '0'
            else:
                out_str += '1'

            mask = mask << 1 # Move to the next bit

        return out_str[::-1]

    def to_int(self):
        return int.from_bytes(self.to_bytes(), 'big')

    def from_int(self, i: int):
        if 0 <= i <= 1:
            l = 1
        else:
            l = math.ceil(math.log(i, 2)/8)
        return self.from_bytes(i.to_bytes(l, 'big'))


    def index(self, s: str):
        return self.bin_string.index(s)

    # Operator overloads
    def __iter__(self):
        return self

    def __next__(self):
        if self.iter_i >= len(self):
            self.iter_i = 0
            raise StopIteration
        result = self.bin_string[self.iter_i]
        self.iter_i += 1
        return result

    def __add__(self, other):
        if not type(other) is BString:
            raise TypeError('unsupported operand type(s) for +: BString and {}'.format(type(other)))
        return BString(self.__str__() + str(other))

    def __int__(self):
        return self.to_int()

    def __str__(self):
        return self.bin_string

    def __repr__(self):
        return self.__str__()

    def __len__(self):
        return len(self.__str__())

    def __getitem__(self, k: int):
        return BString(self.__str__()[k])

    def __setitem__(self, k: int, item: str):
        assert item == '0' or item == '1', 'set item must be a 0 or 1'
        new_str = list(self.__str__())
        new_str[k] = item
        self.bin_string = ''.join(new_str)

    def __rshift__(self, shamt: int):
        new_str = self.__str__()
        for i in range(shamt):
            new_str = new_str[-1] + new_str[:-1]
        return BString(new_str)

    def __lshift__(self, shamt: int):
        new_str = self.__str__()
        for i in range(shamt):
            new_str = new_str[1:] + new_str[0]
        return BString(new_str)

    def __xor__(self, other):
        if not type(other) is BString:
            raise TypeError('unsupported operand type(s) for ^: BString and {}'.format(type(other)))
        assert len(self) == len(other), 'length of input must be same as length of self'
        new_str = ''

        for i, c in enumerate(self):
            new_str += str(int(c) ^ int(other[i]))

        return BString(new_str)
